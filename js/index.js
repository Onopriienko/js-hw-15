//Скрыть и показать секцию
$(".hide-show").click(function () {
    $(".news-container").slideToggle("slow");
});

//Кнопка вврех экрана

const btn = $('#button-up');
const height = $(window).height();

$(window).scroll(function() {
    if ($(window).scrollTop() > height) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});
btn.on('click', function() {
    $('html, body').animate({scrollTop:0}, 1500);
});

//Переход по секциям

    $("#menu").on("click","a", function () {
        const id  = $(this).attr('href');
        const top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 2000);
    });